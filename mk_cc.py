def make_cc ():

    import random

    bank_prefixes = ['4','51','52','53','54','55','38','38','6011','65','35','34','37']

    cc = random.choice(bank_prefixes)

    while len(cc) < 15:
      
      cc += str(random.choice([0,1,2,3,4,5,6,7,8,9,0]))

    checksum = 0

    for i in range(len(cc)):

      if (i%2) == 0:
        checksum += int(2*int(cc[i])/10) + 2*int(cc[i])%10
      else:
        checksum += int(cc[i])

    cc += str(10 - (checksum%10))

    return cc

if __name__ == '__main__':

   print(make_cc())